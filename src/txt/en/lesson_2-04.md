# 2.4. Mis no-lugares

> **OJO.** Si vienes de `$ … 2.3`, ejecuta `$ hackpublishing go 2.3` y vuelve
> a leer con detenimiento.

Si vienes de `$ … 2.5` ya sabes moverte entre las lecciones, incluyendo dar
saltos hacia delante o para atrás.

Además viste que la instrucción completa de `$ hackpublishing go 2.4` es igual
a `$ … 2.4`. Por convención, en este manual abreviaré `hackpublishing` y
`hackpublishing go` con `…`, ¿vale?

Dar saltos en tu terminal no es peligroso. Si intentas `$ … 2.14` te imprimirá
error. Si intentas ir a un no-lugar, la terminal te indicará su imposibilidad.
¿Ya lo intentaste?

En Hackedición, como en la vida, errar no es un problema, sino un aprendizaje.
¿Que te parece si ahora vas a `$ … 2.6`?
