# 2.2. Mi terminal y mi prompt

¡Hola de nuevo! Como has visto, al escribir `$ hackpublishing go` en tu ==terminal==
(esta pantalla con puro texto que sirve para ejecutar programas) en realidad no
tienes que escribir `$`. Los símbolos `$` y `#` son una convención para señalar
el final de tu ==_prompt_==.

El _prompt_ son los caracteres impresos en tu terminal para indicar la espera de
tus órdenes de ejecución. Estos caracteres son información muy relevante. Según
tu configuración, el _prompt_ provee alguno de estos datos:

- El nombre de tu usuario para autentificación.
- El nombre de tu computadora para establecer conexión con otras máquinas.
- El directorio de trabajo actual para indicar tu ubicación.

Tu terminal, como tu explorador de carpetas, siempre está en un directorio.
Ahora mismo estás ubicado adentro de la carpeta `study-room`, el directorio
destinado para tu sala de estudio. Anda, lee tu _prompt_ para corroborarlo.

Tal vez ahorita esto sea un tanto confuso. No te preocupes, la práctica devela
muchos misterios… ¿Por qué no avanzas como ya sabes?
