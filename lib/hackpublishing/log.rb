require 'hackpublishing/common'

module Hackpublishing
  class Log

    def initialize
      # Prints only if `hackpublishing log`
      if caller.first =~ /exe\/hackpublishing/ && ARGV.length == 1
        self.print
      end
    end

    # Catches user input
    def catch par
      if par == 'clean' then self.clean else prt_help end
    end

    # Creates log file
    def create path
      file  = File.open(path + '/' + $logf, 'w:utf-8')
      file.close
    end

    # Saves log file
    def save c, content, path = get_log_path
      if File.exists?(path)
        log  = File.read(path).strip
        file = File.open(path, 'w:utf-8')
        file.puts log
        file.puts '[' + Time.now.to_s + '] ' + c.class.to_s  + ' => ' + content.to_s
        file.close
      end
    end

    # Prints log
    def print
      $log.save(self, __method__)
      inside?(3)
      puts File.read(get_log_path)
    end

    # Cleans log
    def clean
      $log.save(self, __method__)
      inside?(3)
      File.delete(get_log_path)
      self.create(File.dirname(get_log_path))
    end
  end
end
