# Extends True class
class TrueClass
  def to_box
    '  [x] '
  end
end

# Extends False class
class FalseClass
  def to_box
    '  [ ] '
  end
end

# Extends String class
class String

# TODO: make test with StringScanner

  # Converts par to @
  def to_p par
    r = /\W#{par}+\S(.|\n)+?\S#{par}+\W/
    m = self.match(r)
    m ? self.gsub!(r,){|e| e.gsub(/#{par}/, '¶')} : self
  end

  # Converts MD no-code block to TTY
  def to_tty
    self.to_p('_').to_p('\*')
      .gsub(/¶¶(\S(.|\n)+?\S)¶¶/, '\1'.bold)
      .gsub(/¶(\S(.|\n)+?\S)¶/, '\1'.italic)
      .gsub(/\[(.+?)\]\((.+?)\)/, '\1 <\2>'
        .colorize(:cyan))
      .gsub(/^>\s*/, ' '
        .colorize(:background => :yellow) + ' ')
      .gsub(/^(#+)\s*(.+?)$/, ('\1'.hide + '\2 ')
        .colorize(:color => :white, :background => :magenta))
      .gsub(/==(\S(.|\n)+?\S)==/, '\1'
        .colorize(:color => :black, :background => :light_yellow))
      .gsub(/`(\S(.|\n)*?)`(\W|\s)/, '\1'
        .colorize(:color => :black, :background => :light_cyan) + '\3')
  end

  # Converts MD code block to TTY
  def to_tty_code
    self
      .gsub(/^``` *\n*/, '')
      .split(/\n+/)
      .join("\n")
      .colorize(:light_magenta)
  end
end
